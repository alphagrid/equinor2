module.exports = function (api) {
  
  api.cache(true);
  
  const presets = [
    [
      '@babel/preset-env',
      {
        modules: 'auto',
        useBuiltIns: 'usage',
        debug: false,
        corejs: '3',
        targets: {
          'ie': '11'
        }
      }
    ],
    ["@babel/react", {}]
  ];

  return {
    presets
  }

}