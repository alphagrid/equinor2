import React, {Fragment} from 'react'
import html from 'react-inner-html';

import { gsap } from "gsap/dist/gsap";
import {EasePack, DrawSVGPlugin} from 'gsap/all'
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

import s from './index.css';

// import Particles from "react-tsparticles";

const EL0 = `.eq2-gw`
const ST0 = {
  trigger: `.eq2`,
  start: "bottom bottom",
  end: "top top",
  scrub: 0,
  markers: false
}

const EL1 = `.eq2-waves`
const ST1 = {
  trigger: `.eq2`,
  start: "top bottom",
  end: "bottom top",
  scrub: 0,
  markers: false
}

const EL2 = `.eq2-year`
const ST2 = {
  trigger: `.eq2`,
  start: "bottom bottom",
  end: "top top",
  scrub: 0,
  markers: false
}


const EL4 = `.eq2-turbine1`
const EL5 = `.eq2-turbine2`
const EL6 = `.eq2-turbine3`
const EL7 = `.eq2-turbine4`
const ST4 = {
  trigger: `.eq2`,
  start: "top bottom",
  end: "bottom top",
  scrub: 0.5,
  markers: false
}

const EL8 = `.eq2-wind0`
const ST8 = {
  trigger: `.eq2`,
  start: "top 50%",
  end: "bottom 30%",
  scrub: 0,
  markers: false
}
const EL9 = `.eq2-wind1`
const ST9 = {
  trigger: `.eq2`,
  start: "top 70%",
  end: "200% 50%",
  scrub: 0,
  markers: false
}


const ST10 = {
  trigger: `.eq2`,
  start: "center bottom",
  end: "center top",
  scrub: 0,
  markers: false
}


export default class Eq2 extends React.Component {
  constructor(props) {
    super(props);
   
    gsap.defaults({overwrite: "true"});
    gsap.registerPlugin(ScrollTrigger);
    gsap.registerPlugin(DrawSVGPlugin);

    this.state = {
      value: 0
    }

    this.particlesInit = this.particlesInit.bind(this);
    this.particlesLoaded = this.particlesLoaded.bind(this);
  }

  componentDidMount() {

      let counter = { var: 10 };
      const tl0 = gsap.timeline({scrollTrigger: ST0});
      tl0.to(counter, {var: 40, duration: 1, ease: `none`, 
       onUpdate: function () {
         document.querySelector(EL0).textContent = Math.ceil(counter.var) + `GW`;
       }, 
     }, 0);

      let counter2 = { var: 2021 };
      const tl2 = gsap.timeline({scrollTrigger: ST2});
      tl2.to(counter2, {var: 2030, duration: 1, ease: `none`, 
       onUpdate: function () {
         document.querySelector(EL2).textContent = Math.ceil(counter2.var);
       }, 
     }, 0);

      gsap.set(EL1, {x: 0});
      this.tl1 = gsap.timeline({scrollTrigger: ST1});
      this.tl1.set(EL1, {x: 0});
      this.tl1.to(EL1, {x: 80, duration: 1});


      gsap.set(EL4, {rotation:"+=30", scale: 0.7, transformOrigin: '49.5% 67.5%'});
      this.tl4 = gsap.timeline({scrollTrigger: ST4});
      this.tl4.set(EL4, {rotation:"+=30", scale: 0.7, });
      this.tl4.to(EL4, {rotation:"+=720", scale: 1.1, duration: 1});

      gsap.set(EL5, {rotation:"+=0", scale: 0.7, transformOrigin: '49.5% 67.5%'});
      this.tl5 = gsap.timeline({scrollTrigger: ST4});
      this.tl5.set(EL5, {rotation:"+=0", scale: 0.7, });
      this.tl5.to(EL5, {rotation:"+=720", scale: 1.1, duration: 1});

      gsap.set(EL6, {rotation:"+=30", scale: 0.7, transformOrigin: '49.5% 67.5%'});
      this.tl6 = gsap.timeline({scrollTrigger: ST4});
      this.tl6.set(EL6, {rotation:"+=30", scale: 0.7, });
      this.tl6.to(EL6, {rotation:"+=720", scale: 1.1, duration: 1});

      gsap.set(EL7, {rotation:"+=0", scale: 0.7, transformOrigin: '49.5% 67.5%'});
      this.tl7 = gsap.timeline({scrollTrigger: ST4});
      this.tl7.set(EL7, {rotation:"+=0", scale: 0.7, });
      this.tl7.to(EL7, {rotation:"+=720", scale: 1.1, duration: 1});


      gsap.set(EL8, {drawSVG:"0% 0%"});
      this.tl8 = gsap.timeline({scrollTrigger: ST8});
      this.tl8.set(EL8, {drawSVG:"0% 0%" });
      this.tl8.to(EL8, {drawSVG:"0% 100%", duration: 0.5});
      this.tl8.to(EL8, {drawSVG:"100% 100%", duration: 0.5});

      gsap.set(EL9, {drawSVG:"0% 0%"});
      this.tl9 = gsap.timeline({scrollTrigger: ST9});
      this.tl9.set(EL9, {drawSVG:"0% 0%" });
      this.tl9.to(EL9, {drawSVG:"0% 100%", duration: 0.5});
      this.tl9.to(EL9, {drawSVG:"100% 100%", duration: 0.5});


      const that = this;
      let counter3 = { var: 0 };
      const tl10 = gsap.timeline({scrollTrigger: ST10});
      tl10.to(counter3, {var: 5, duration: 1, ease: `none`,
       onUpdate: function () {

          // console.log(counter3.var)

          let nv = Math.round(counter3.var)



          if(nv!==that.state.value){

            gsap.set(`.eq-turbo`, {opacity: ((nv>=3)?1:0)});
            that.setState({value: nv});

          }
       }, 
      }, 0);


      gsap.to(`.eq-particles-h`, {x: 20, duration: 1, repeat: -1, ease: `none`});
      gsap.to(`.eq-particles-v1`, {y: 20, duration: 1, repeat: -1, ease: `none`});
      gsap.to(`.eq-particles-v2`, {y: -20, duration: 1, repeat: -1, ease: `none`});




  }

  componentDidUpdate(prevProps, prevState, snapshot){




  }

 particlesInit(main) {
    // console.log(main);

    // you can initialize the tsParticles instance (main) here, adding custom shapes or presets
  }

  particlesLoaded(container) {
    // console.log(container);
  }

  render() {

    // console.log(`********************`)
    // console.log(this.state.value)
    // console.log(`********************`)

    return (

        <div className={`eq2c`}>

          <div className={`eq2`} {...html(require(`!raw-loader!../../assets/eq2.svg`))} />

{/*
         <Particles
            id="tsparticles"
            init={this.particlesInit}
            loaded={this.particlesLoaded}
            options={{
              background: {
                color: {
                  value: "transparent",
                },
              },
              fpsLimit: 60,

              particles: {
                color: {
                  value: "#fff",
                },
                collisions: {
                  enable: false,
                },
                move: {
                  direction: "right",
                  enable: true,
                  outMode: "out",
                  random: false,
                  speed: 1 + ((this.state.value)?this.state.value:0),
                  straight: false,
                },
                number: {
                  density: {
                    enable: false,
                    value_area: 100,
                  },
                  value: 70 + ((this.state.value)?this.state.value:0)*70,
                },
                opacity: {
                  value: 1,
                },
                shape: {
                  type: "circle",
                  stroke: {
                    width: 1,
                    color: "#2e6e77"
                  }
                },
                size: {
                  random: false,
                  value: 1,
                },
              },
              detectRetina: true,
            }}
          />

        <svg className="svgmask" width="1183" height="276" viewBox="0 0 1183 276">
          <defs>
            <clipPath id="clippath2">
              <path 
                transform="translate(2 1)"
                d="M841.9,250.4H388.8c-8.9,0-16.2-7.2-16.2-16.2v-62.7h13.9v62.7c0,1.3,1,2.3,2.3,2.3h453c1.3,0,2.3-1,2.3-2.3v-59.7H858v59.7C858,243.2,850.8,250.4,841.9,250.4z" 
                fill="#ffcc00"
              />
            </clipPath>
          </defs>
        </svg>
*/}

        </div>

    );
  }
}
