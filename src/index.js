import s from './reset.css';
import './index.css';

import React from 'react';
import ReactDOM from 'react-dom';

import Eq1 from './components/eq1';
import Eq2 from './components/eq2';
import Eq3 from './components/eq3';
import Eq4 from './components/eq4';

function ready() {
	const equinor1 = document.getElementById('equinor1');
	const equinor2 = document.getElementById('equinor2');
	const equinor3 = document.getElementById('equinor3');
	const equinor4 = document.getElementById('equinor4');

	if(equinor1){ ReactDOM.render(<Eq1 />, equinor1); }
	if(equinor2){ ReactDOM.render(<Eq2 />, equinor2); }
	if(equinor3){ ReactDOM.render(<Eq3 />, equinor3); }
	if(equinor4){ ReactDOM.render(<Eq4 />, equinor4); }
}

document.addEventListener("DOMContentLoaded", ready);