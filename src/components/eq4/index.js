import React, {Fragment} from 'react'
import html from 'react-inner-html';

import { gsap } from "gsap/dist/gsap";
import {EasePack} from 'gsap/all'
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

import s from './index.css';


const EL0 = `.eq4-man1`;
const ST0 = {
  trigger: `.eq4`,
  start: "top bottom",
  end: "bottom 60%",
  scrub: 0,
  markers: false
}

const EL1 = `.eq4-man2`;
const ST1 = {
  trigger: `.eq4`,
  start: "top bottom",
  end: "bottom 50%",
  scrub: 0,
  markers: false
}

const EL2 = `.eq4-man3`;
const ST2 = {
  trigger: `.eq4`,
  start: "top bottom",
  end: "bottom 70%",
  scrub: 0,
  markers: false
}

const EL3 = `.eq4-text`;
const ST3 = {
  trigger: `.eq4`,
  start: "bottom bottom",
  end: "top top",
  scrub: 0,
  markers: false
}


const EL4 = `.eq4-line`;
const ST4 = {
  trigger: `.eq4`,
  start: "bottom bottom",
  end: "top top",
  scrub: 0,
  markers: false
}

const EL5 = `.eq4-two`;
const ST5 = {
  trigger: `.eq4`,
  start: "top bottom",
  end: "top top",
  scrub: 0,
  markers: false
}

export default class Eq4 extends React.Component {
  constructor(props) {
    super(props);
   
    gsap.defaults({overwrite: "true"});
    gsap.registerPlugin(ScrollTrigger);

    this.state = {
    }

  }

  componentDidMount() {

      gsap.set(EL0, {x: -20, opacity: 0});
      this.tl0 = gsap.timeline({scrollTrigger: ST0});
      this.tl0.set(EL0, {x: -20, opacity: 0});
      this.tl0.to(EL0, {x: 0, opacity: 1, duration: 1});

      gsap.set(EL1, {y: 50, opacity: 0});
      this.tl1 = gsap.timeline({scrollTrigger: ST1});
      this.tl1.set(EL1, {y: 50, opacity: 0});
      this.tl1.to(EL1, {y: 0, opacity: 1, duration: 1});

      gsap.set(EL2, {x: 30, opacity: 0});
      this.tl2 = gsap.timeline({scrollTrigger: ST2});
      this.tl2.set(EL2, {x: 30, opacity: 0});
      this.tl2.to(EL2, {x: 0, opacity: 1, duration: 1});

      gsap.set(EL3, {x: 0, opacity: 0});
      this.tl3 = gsap.timeline({scrollTrigger: ST3});
      this.tl3.set(EL3, {x: 0, opacity: 0});
      this.tl3.to(EL3, {x: 0, opacity: 1, duration: 1});
      
      gsap.set(EL4, {scaleX: 0, transformOrigin: '100% 50%'});
      this.tl4 = gsap.timeline({scrollTrigger: ST4});
      this.tl4.set(EL4, {scaleX: 0});
      this.tl4.to(EL4, {scaleX: 1, duration: 1});


      gsap.set(EL5, {x: 50, opacity: 1});
      this.tl5 = gsap.timeline({scrollTrigger: ST5});
      this.tl5.set(EL5, {x: 50, opacity: 1});
      this.tl5.to(EL5, {x: 0, opacity: 1, duration: 1});


  }

  componentDidUpdate(prevProps, prevState, snapshot){
  }

  handleResize(){
  }

  handleScroll(){
  }

  render() {

    return (
        <Fragment>
          <div className={`eq4`} {...html(require(`!raw-loader!../../assets/eq4.svg`))} />
        </Fragment>
    );
  }
}
