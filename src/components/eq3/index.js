import React, {Fragment} from 'react'
import html from 'react-inner-html';

import { gsap } from "gsap/dist/gsap";
import {EasePack} from 'gsap/all'
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

import s from './index.css';

const EL0 = `.eq3-value`;
const ST0 = {
  trigger: `.eq3`,
  start: "bottom bottom",
  end: "top top",
  scrub: 0,
  markers: false
}

const EL1 = `.eq3-5gw`;
const ST1 = {
  trigger: `.eq3`,
  start: "center bottom",
  end: "top top",
  scrub: 0,
  markers: false
}

const EL2 = `.eq3-arrow`;
const ST2 = {
  trigger: `.eq3`,
  start: "center bottom",
  end: "top top",
  scrub: 0,
  markers: false
}

const EL3 = `.eq3-line`;
const ST3 = {
  trigger: `.eq3`,
  start: "center bottom",
  end: "top top",
  scrub: 0,
  markers: false
}

const EL4 = `.eq3-back`;
const ST4 = {
  trigger: `.eq3`,
  start: "top bottom",
  end: "bottom top",
  scrub: 0,
  markers: false
}






export default class Eq3 extends React.Component {
  constructor(props) {
    super(props);
   
    gsap.defaults({overwrite: "true"});
    gsap.registerPlugin(ScrollTrigger);

    this.state = {
    }

  }

  componentDidMount() {

      let counter = { var: 0 };
      const tl0 = gsap.timeline({scrollTrigger: ST0});
      tl0.to(counter, {var: 4626, duration: 1, 
       onUpdate: function () {
         document.querySelector(EL0).textContent = Math.ceil(counter.var) + `%`;
       }, 
     }, 0);


      gsap.set(EL1, {y: 20, opacity: 0});
      this.tl1 = gsap.timeline({scrollTrigger: ST1});
      this.tl1.set(EL1, {y: 20, opacity: 0});
      this.tl1.to(EL1, {y: -50, opacity: 1, duration: 1});

      gsap.set(EL2, {y: 20});
      this.tl2 = gsap.timeline({scrollTrigger: ST2});
      this.tl2.set(EL2, {y: 20});
      this.tl2.to(EL2, {y: -50, duration: 1});

      gsap.set(EL3, {scaleY: 0.7, transformOrigin: 'center bottom'});
      this.tl3 = gsap.timeline({scrollTrigger: ST3});
      this.tl3.set(EL3, {scaleY: 0.7});
      this.tl3.to(EL3, {scaleY: 1.8, duration: 1});

      gsap.set(EL4, {y: 0});
      this.tl4 = gsap.timeline({scrollTrigger: ST4});
      this.tl4.set(EL4, {y: 0});
      this.tl4.to(EL4, {y: 30, duration: 1});



  }

  componentDidUpdate(prevProps, prevState, snapshot){
  }

  handleResize(){
  }

  handleScroll(){
  }

  render() {

    return (
        <Fragment>
          <div className={`eq3`} {...html(require(`!raw-loader!../../assets/eq3.svg`))} />
        </Fragment>
    );
  }
}
