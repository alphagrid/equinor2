import React, {Fragment} from 'react'
import html from 'react-inner-html';

import { gsap } from "gsap/dist/gsap";
import {EasePack} from 'gsap/all'
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

import s from './index.css';



const values = [0,6,16,41,68, 78]


export default class Eq1 extends React.Component {
  constructor(props) {
    super(props);
   
    gsap.defaults({overwrite: "true"});
    gsap.registerPlugin(ScrollTrigger);

    this.state = {
      value:0
    }
    this.setValue = this.setValue.bind(this);
  }

  componentDidMount() {
  }

  componentDidUpdate(prevProps, prevState, snapshot){

    const txt = document.querySelector(`#eq1-value`)
    if(this.state.value!==prevState.value){
      let counter = { var: values[prevState.value] };
      gsap.to(counter, {var: values[this.state.value], duration: 1, ease: "power2.out", 
        onUpdate: function () {
          let nval = Math.ceil(counter.var);
          if(nval!==0){
            txt.textContent = Math.ceil(counter.var) + `%`;
          }else{
            txt.textContent = `596 MtCO₂`;
          }
        }, 
      });


    }





  }

  handleResize(){
  }

  handleScroll(){
  }

  setValue(id){
    this.setState({value: id});
  }

  render() {

    return (
        <Fragment>
          <div className={`eq1`}>

            <svg xmlns="http://www.w3.org/2000/svg" width="630" height="414" viewBox="0 0 630 414">
              <rect width="626" height="365" transform="translate(1.5)" fill="#f2f9fc"/>
                <g className={`eq1-cloud eq1-cloud${this.state.value}`}>
                  <g transform="translate(-10195.5 -322)">
                    <g transform="translate(9925.563 252.532)">
                      <path d="M 706.7760009765625 362.2131958007813 L 459.583740234375 362.2131958007813 L 459.583740234375 362.020751953125 L 459.3507385253906 362.0049133300781 C 449.2658386230469 361.3169555664063 439.5527954101563 358.7507019042969 430.4814453125 354.37744140625 C 426.1006469726563 352.2654418945313 421.8879699707031 349.7297058105469 417.9603881835938 346.8406066894531 C 414.0609741210938 343.9721984863281 410.4030456542969 340.7265625 407.088134765625 337.1937561035156 C 393.2874450683594 322.4859924316406 385.6870422363281 303.2724609375 385.6870422363281 283.0924072265625 C 385.6870422363281 274.1324462890625 387.1732788085938 265.3414001464844 390.1045227050781 256.9634399414063 C 392.9377746582031 248.8654937744141 397.0601806640625 241.3130950927734 402.357177734375 234.5159454345703 C 407.600830078125 227.787353515625 413.8548278808594 221.9768524169922 420.9454345703125 217.2458953857422 C 428.1488342285156 212.4396514892578 436.0294189453125 208.8748931884766 444.3683471679688 206.6506500244141 L 444.5063781738281 206.6138458251953 L 444.5447387695313 206.4761962890625 C 446.8394775390625 198.2393493652344 450.4460754394531 190.4631958007813 455.2644348144531 183.3637542724609 C 460.0110473632813 176.3699035644531 465.8135375976563 170.2056427001953 472.5107727050781 165.0421447753906 C 479.2788391113281 159.8240051269531 486.786376953125 155.764404296875 494.8248291015625 152.9760437011719 C 503.1408386230469 150.0914459228516 511.863037109375 148.6287994384766 520.7491455078125 148.6287994384766 C 530.584228515625 148.6287994384766 540.2152099609375 150.4324493408203 549.3745727539063 153.9897003173828 L 549.5306396484375 154.05029296875 L 549.6460571289063 153.9290924072266 C 553.24951171875 150.14599609375 557.2398071289063 146.70654296875 561.506103515625 143.7061462402344 C 565.8124389648438 140.6777038574219 570.432373046875 138.0740966796875 575.2376098632813 135.9677429199219 C 580.1334838867188 133.8215942382813 585.2664184570313 132.1725006103516 590.4937744140625 131.0661926269531 C 595.8623046875 129.9300994873047 601.3919067382813 129.35400390625 606.928955078125 129.35400390625 C 617.3994750976563 129.35400390625 627.5708618164063 131.3671569824219 637.1604614257813 135.3374481201172 C 641.7719116210938 137.2467498779297 646.228759765625 139.6118469238281 650.4069213867188 142.3670959472656 C 654.5438842773438 145.0951538085938 658.4473266601563 148.2315521240234 662.0088500976563 151.6891479492188 C 665.5675048828125 155.14404296875 668.8169555664063 158.9507446289063 671.6668090820313 163.0036010742188 C 674.5406494140625 167.0906524658203 677.0352783203125 171.4667510986328 679.0813598632813 176.0104064941406 C 683.3287963867188 185.4423522949219 685.6514282226563 195.4993438720703 685.9847412109375 205.9021453857422 L 685.9945068359375 206.208251953125 L 686.2924194335938 206.1373443603516 C 692.3363647460938 204.6996459960938 698.5151977539063 203.9706573486328 704.6572265625 203.9706573486328 C 710.0257568359375 203.9706573486328 715.3908081054688 204.5115051269531 720.6033325195313 205.5780944824219 C 725.6832885742188 206.6175994873047 730.6800537109375 208.168701171875 735.4548950195313 210.1882934570313 C 740.14306640625 212.1712493896484 744.6648559570313 214.6255950927734 748.8945922851563 217.4832000732422 C 753.084228515625 220.3136444091797 757.02392578125 223.5643005371094 760.6043701171875 227.1447448730469 C 764.1847534179688 230.7252502441406 767.435302734375 234.6650543212891 770.2658081054688 238.8547058105469 C 773.123291015625 243.0845031738281 775.5775756835938 247.6063537597656 777.5604858398438 252.2946014404297 C 779.5800170898438 257.0694580078125 781.131103515625 262.0663146972656 782.1705932617188 267.146240234375 C 783.2371826171875 272.3588562011719 783.7779541015625 277.7239074707031 783.7779541015625 283.0924072265625 C 783.7779541015625 293.5802001953125 781.7583618164063 303.7670593261719 777.7752685546875 313.3699951171875 C 775.8599243164063 317.9877624511719 773.487548828125 322.4497375488281 770.7238159179688 326.6321411132813 C 767.9874267578125 330.7733459472656 764.8416137695313 334.6796875 761.3739624023438 338.2427062988281 C 757.9087524414063 341.8031005859375 754.0911254882813 345.052490234375 750.0270385742188 347.9007568359375 C 745.9282836914063 350.7732543945313 741.5401611328125 353.2645568847656 736.9846801757813 355.3055114746094 C 727.5263671875 359.5429382324219 717.4443359375 361.8468017578125 707.0186157226563 362.1530456542969 L 706.7760009765625 362.1601867675781 L 706.7760009765625 362.2131958007813 Z" fill="#fff" stroke="#2e6e77" strokeMiterlimit="10"/>
                    </g>
                    <text transform="translate(10400.5 558.636)" fill="#73afb6" stroke="#007079" strokeWidth="0.5" fontSize="137" fontFamily="FinancierDisplay-Bold, Financier Display" fontWeight="700">
                      <tspan x="107" y="0" id="eq1-value" textAnchor="middle">596 MtC0₂</tspan>
                    </text>
                  </g>
                </g>

              <g transform="translate(-10195.5 -322)">

                <path d="M10983.72,431.995h626" transform="translate(-786.22 256)" fill="none" stroke="#2e6e77" strokeLinecap="round" strokeWidth="4" opacity="0.47"/>
                <path d="M10984.72,431.995" transform="translate(-568.72 256)" fill="none" stroke="#eb384a" strokeLinecap="round" strokeWidth="4"/>
                <path d="M10983.72,431.995h1" transform="translate(-188.72 256)" fill="none" stroke="#eb384a" strokeLinecap="round" strokeWidth="4"/>

                <text transform="translate(10209 733)" fill="#253746" fontSize="16" fontFamily="Metric-Medium, Metric" fontWeight="500" letterSpacing="-0.021em"><tspan x="0" y="0">1990</tspan></text>
                <text transform="translate(10322 733)" fill="#253746" fontSize="16" fontFamily="Metric-Medium, Metric" fontWeight="500" letterSpacing="-0.021em"><tspan x="0" y="0">2000</tspan></text>
                <text transform="translate(10438 733)" fill="#253746" fontSize="16" fontFamily="Metric-Medium, Metric" fontWeight="500" letterSpacing="-0.021em"><tspan x="0" y="0">2010</tspan></text>
                <text transform="translate(10553 733)" fill="#253746" fontSize="16" fontFamily="Metric-Medium, Metric" fontWeight="500" letterSpacing="-0.021em"><tspan x="0" y="0">2019</tspan></text>
                <text transform="translate(10666 733)" fill="#253746" fontSize="16" fontFamily="Metric-Medium, Metric" fontWeight="500" letterSpacing="-0.021em"><tspan x="0" y="0">2030</tspan></text>
                <text transform="translate(10781 733)" fill="#253746" fontSize="16" fontFamily="Metric-Medium, Metric" fontWeight="500" letterSpacing="-0.021em"><tspan x="0" y="0">2035</tspan></text>

                <g className={`eq1-btn`} onClick={() => {this.setValue(0);}} >
                  
                  <g transform="translate(-574 0)" className={`eq1-btnarea${(this.state.value===0)?" eq1-btnarea-active":""}`}>
                    <g transform="translate(141 -30)">
                      <g transform="translate(10630 693)" fill="#d5eaf4" stroke="#2e6e77" strokeWidth="0.2" opacity="0.458">
                        <circle cx="25" cy="25" r="25" stroke="none"/>
                        <circle cx="25" cy="25" r="24.9" fill="none"/>
                      </g>
                      <circle cx="9" cy="9" r="9" transform="translate(10646 709)" fill="#2e6e77" opacity="0.666"/>
                    </g>
                  </g>

                  <g transform="translate(10217 683)" fill="#fff" stroke="#2e6e77" strokeWidth="1">
                    <circle cx="5" cy="5" r="5" stroke="none"/>
                    <circle cx="5" cy="5" r="4.5" fill="none"/>
                  </g>

                </g>

                <g className={`eq1-btn`} onClick={() => {this.setValue(1);}} >
                  
                  <g transform="translate(-460 0)" className={`eq1-btnarea${(this.state.value===1)?" eq1-btnarea-active":""}`}>
                    <g transform="translate(141 -30)">
                      <g transform="translate(10630 693)" fill="#d5eaf4" stroke="#2e6e77" strokeWidth="0.2" opacity="0.458">
                        <circle cx="25" cy="25" r="25" stroke="none"/>
                        <circle cx="25" cy="25" r="24.9" fill="none"/>
                      </g>
                      <circle cx="9" cy="9" r="9" transform="translate(10646 709)" fill="#2e6e77" opacity="0.666"/>
                    </g>
                  </g>

                  <g transform="translate(10331 683)" fill="#fff" stroke="#2e6e77" strokeWidth="1">
                    <circle cx="5" cy="5" r="5" stroke="none"/>
                    <circle cx="5" cy="5" r="4.5" fill="none"/>
                  </g>

                </g>

                <g className={`eq1-btn`} onClick={() => {this.setValue(2);}} >

                  <g transform="translate(-345 0)" className={`eq1-btnarea${(this.state.value===2)?" eq1-btnarea-active":""}`}>
                    <g transform="translate(141 -30)">
                      <g transform="translate(10630 693)" fill="#d5eaf4" stroke="#2e6e77" strokeWidth="0.2" opacity="0.458">
                        <circle cx="25" cy="25" r="25" stroke="none"/>
                        <circle cx="25" cy="25" r="24.9" fill="none"/>
                      </g>
                      <circle cx="9" cy="9" r="9" transform="translate(10646 709)" fill="#2e6e77" opacity="0.666"/>
                    </g>
                  </g>

                  <g transform="translate(10446 683)" fill="#fff" stroke="#2e6e77" strokeWidth="1">
                    <circle cx="5" cy="5" r="5" stroke="none"/>
                    <circle cx="5" cy="5" r="4.5" fill="none"/>
                  </g>

                </g>

                <g className={`eq1-btn`} onClick={() => {this.setValue(3);}} >
                  
                  <g transform="translate(-230 0)" className={`eq1-btnarea${(this.state.value===3)?" eq1-btnarea-active":""}`}>
                    <g transform="translate(141 -30)">
                      <g transform="translate(10630 693)" fill="#d5eaf4" stroke="#2e6e77" strokeWidth="0.2" opacity="0.458">
                        <circle cx="25" cy="25" r="25" stroke="none"/>
                        <circle cx="25" cy="25" r="24.9" fill="none"/>
                      </g>
                      <circle cx="9" cy="9" r="9" transform="translate(10646 709)" fill="#2e6e77" opacity="0.666"/>
                    </g>
                  </g>

                  <g transform="translate(10561 683)" fill="#fff" stroke="#2e6e77" strokeWidth="1">
                    <circle cx="5" cy="5" r="5" stroke="none"/>
                    <circle cx="5" cy="5" r="4.5" fill="none"/>
                  </g>

                </g>

                <g className={`eq1-btn`} onClick={() => {this.setValue(4);}} >

                  <g transform="translate(-115 0)" className={`eq1-btnarea${(this.state.value===4)?" eq1-btnarea-active":""}`}>
                    <g transform="translate(141 -30)">
                      <g transform="translate(10630 693)" fill="#d5eaf4" stroke="#2e6e77" strokeWidth="0.2" opacity="0.458">
                        <circle cx="25" cy="25" r="25" stroke="none"/>
                        <circle cx="25" cy="25" r="24.9" fill="none"/>
                      </g>
                      <circle cx="9" cy="9" r="9" transform="translate(10646 709)" fill="#2e6e77" opacity="0.666"/>
                    </g>
                  </g>

                  <g transform="translate(10676 683)" fill="#fff" stroke="#2e6e77" strokeWidth="1">
                    <circle cx="5" cy="5" r="5" stroke="none"/>
                    <circle cx="5" cy="5" r="4.5" fill="none"/>
                  </g>

                </g>

                <g className={`eq1-btn`} onClick={() => {this.setValue(5);}} >

                  <g transform="translate(0 0)" className={`eq1-btnarea${(this.state.value===5)?" eq1-btnarea-active":""}`}>
                    <g transform="translate(141 -30)">
                      <g transform="translate(10630 693)" fill="#d5eaf4" stroke="#2e6e77" strokeWidth="0.2" opacity="0.458">
                        <circle cx="25" cy="25" r="25" stroke="none"/>
                        <circle cx="25" cy="25" r="24.9" fill="none"/>
                      </g>
                      <circle cx="9" cy="9" r="9" transform="translate(10646 709)" fill="#2e6e77" opacity="0.666"/>
                    </g>
                  </g>

                  <g transform="translate(10791 683)" fill="#fff" stroke="#2e6e77" strokeWidth="1">
                    <circle cx="5" cy="5" r="5" stroke="none"/>
                    <circle cx="5" cy="5" r="4.5" fill="none"/>
                  </g>

                </g>



                <g className={`eq1-text${(this.state.value===0)?" eq1-text-hidden":""}`}>
                  <text transform="translate(10397 644.5)" fill="#253746" fontSize="20" fontFamily="Metric-Medium, Metric" fontWeight="500">
                    <tspan x="0" y="0">BASED ON 596MtCO₂ </tspan>
                    <tspan y="0">in 1990</tspan>
                  </text>
                </g>

              </g>
            </svg>

          </div>
        </Fragment>
    );
  }
}
